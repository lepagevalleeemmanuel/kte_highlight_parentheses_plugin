#include "hlparentheses_config.h"
#include "hlparentheses.h"
#include "ui_config.h"

HLParenthesesConfig::HLParenthesesConfig(QWidget* parent, const QVariantList &args) :
  KCModule(HighlightParenthesesPluginFactory::componentData(),parent,args),ui(new Ui_Config())
{
  ui->setupUi(this);
  m_lPages << ui->m_pParPage << ui->m_pShiftPage << ui->m_pBracketPage;
  for(int mode=0;mode<HL_PAGE_COUNT;mode++) {
    m_lPages[mode]->setMarkers(QString( HLParPluginView::TOKENS[mode][0]).replace("<<","&lt;&lt;"),QString( HLParPluginView::TOKENS[mode][1]).replace("<<","&lt;&lt;"));
    connect(m_lPages[mode],SIGNAL(changed()),this,SLOT(slotChanged()));
  }
}

void HLParenthesesConfig::save()
{
  for (int tab=0;tab<HL_PAGE_COUNT;tab++) {
    KConfigGroup cg(KGlobal::config(), "Highlight Parentheses Plugin");
    cg.writeEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_bg_opacity"), m_lPages[tab]->m_pBackgroundSB->value());
    cg.writeEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_fg_opacity"), m_lPages[tab]->m_pForegroundSB->value());
    cg.writeEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_unif_step" ), m_lPages[tab]->m_StepSB->value()       );
    cg.writeEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_mode"      ), m_lPages[tab]->m_pMultipleColorRB->isChecked()*1 + m_lPages[tab]->m_UnifidColorRB->isChecked()*2);
    cg.writeEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_unifcolor" ), m_lPages[tab]->m_pColorButtonPB->color().name());
    for (int i=0;i<10;i++)
      cg.writeEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_color")+QString::number(i+1)   , m_lPages[tab]->m_lParButtons[i]->color().name());
  }
  emit changed(false);
  HighlightParenthesesPlugin::reload();
}

void HLParenthesesConfig::load()
{
  for (int tab=0;tab<HL_PAGE_COUNT;tab++) {
    KConfigGroup cg(KGlobal::config(), "Highlight Parentheses Plugin");
    m_lPages[tab]->m_pBackgroundSB->setValue     ( cg.readEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_bg_opacity"),HLParPluginView::DEFAULT_CONF[tab][0])         );
    m_lPages[tab]->m_pForegroundSB->setValue     ( cg.readEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_fg_opacity"),HLParPluginView::DEFAULT_CONF[tab][1])         );
    m_lPages[tab]->m_StepSB->setValue            ( cg.readEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_unif_step" ),HLParPluginView::DEFAULT_CONF[tab][3])         );
    m_lPages[tab]->m_pDisabledRB->setChecked     ( cg.readEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_mode")      ,HLParPluginView::DEFAULT_CONF[tab][2] ) == 0    );
    m_lPages[tab]->m_pMultipleColorRB->setChecked( cg.readEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_mode")      ,HLParPluginView::DEFAULT_CONF[tab][2] ) == 1    );
    m_lPages[tab]->m_UnifidColorRB->setChecked   ( cg.readEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_mode")      ,HLParPluginView::DEFAULT_CONF[tab][2] ) == 2    );
    m_lPages[tab]->m_pColorButtonPB->setColor    ( cg.readEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_unifcolor") , "#0051FF") );
    for (int i=1;i<=10;i++)
      m_lPages[tab]->m_lParButtons[i-1]->setColor(cg.readEntry(HLParPluginView::MODE_PREFIXES[tab]+QString("_color")+QString::number(i),HLParPluginView::DEFAULT_COLORS[i-1]));
    m_lPages[tab]->slotReloadPreview();
  }
  emit changed(false);
}

void HLParenthesesConfig::defaults()
{
  for (int tab=0;tab<HL_PAGE_COUNT;tab++) {
    m_lPages[tab]->m_pBackgroundSB->   setValue  (HLParPluginView::DEFAULT_CONF[tab][0]   );
    m_lPages[tab]->m_pForegroundSB->   setValue  (HLParPluginView::DEFAULT_CONF[tab][1]   );
    m_lPages[tab]->m_pMultipleColorRB->setChecked(HLParPluginView::DEFAULT_CONF[tab][2]==1);
    m_lPages[tab]->m_UnifidColorRB->   setChecked(HLParPluginView::DEFAULT_CONF[tab][2]==2);
    m_lPages[tab]->m_StepSB->          setValue  (HLParPluginView::DEFAULT_CONF[tab][3]   );
    m_lPages[tab]->m_pColorButtonPB->  setColor  ("#0051FF"              );
    for (int i=0;i<10;i++)
      m_lPages[tab]->m_lParButtons[i]->setColor(HLParPluginView::DEFAULT_COLORS[i]);
    m_lPages[tab]->slotReloadPreview();
  }
  emit changed(true);
}

void HLParenthesesConfig::slotChanged()
{
  emit changed(true);
}

// kate: space-indent on; indent-width 2; replace-tabs on;
