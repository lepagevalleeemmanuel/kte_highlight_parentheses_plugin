/* This file is part of the KDE libraries
   Copyright (C) 2013 Emmanuel Lepage Vallee <elv1313@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "configpage.h"
#include "hlparentheses.h"

ConfigPage::ConfigPage(QWidget* parent) : QWidget(parent) {
  setupUi(this);
  m_pColorTW->horizontalHeader()->setResizeMode(0,QHeaderView::Stretch);
  m_pColorTW->horizontalHeader()->setResizeMode(1,QHeaderView::ResizeToContents);
  m_pColorTW->setStyleSheet(QString("QTableView::item {background-color: %1;border:0px}\nQTableView::item::alternate {background-color: %2;border:0px}").arg(
    HLParPluginView::mixColor("#888888",0).name() ).arg( HLParPluginView::mixColor("#888888",0.01).name() ));
  for (int i=0;i<10;i++) {
    m_pColorTW->setRowHeight(i,24);
    KColorButton* button = new KColorButton(Qt::black,this);
    button->setEnabled(false);
    m_pColorTW->setCellWidget(i,1,button);
    m_lParButtons << button;
    QLabel* lbl = new QLabel();
    m_pColorTW->setCellWidget(i,0,lbl);
    m_lPreviewLabel << lbl;
    connect(button,SIGNAL(changed(QColor)),this,SLOT(slotChanged()));
    connect(button,SIGNAL(changed(QColor)),this,SLOT(slotReloadPreview()));
    connect(m_pMultipleColorRB,SIGNAL(toggled(bool)),button,SLOT(setEnabled(bool)));
  }
  m_UnifidColorRB->setChecked(true);//Force the signals to be emmitted
}

void ConfigPage::slotReloadPreview(){
  for(int i=0;i<10;i++)
    if (m_pMultipleColorRB->isChecked())
      m_lPreviewLabel[i]->setText(QString("&nbsp;&nbsp;<font style='background-color:%3;border-radius:5px'>&nbsp;&nbsp;<font style='background-color:%1'><b style='color:%2'>%4</b> code <b style='color:%2'>%5</b></font>&nbsp;&nbsp;</font>").arg(
        HLParPluginView::mixColor(m_lParButtons[i]->color(),m_pBackgroundSB->value()/100.0f).name()).arg(
        HLParPluginView::mixColor(m_lParButtons[i]->color(),m_pForegroundSB->value()/100.0f).name()).arg(
        HLParPluginView::mixColor((!i)?"#000000":m_lParButtons[i-1]->color(),(!i)?0:(m_pBackgroundSB->value()/100.0f)).name()).arg(m_Beg).arg(m_End));
    else if (m_UnifidColorRB->isChecked())
        m_lPreviewLabel[i]->setText(QString("&nbsp;&nbsp;<font style='background-color:%2;border-radius:5px'>&nbsp;&nbsp;<font style='background-color:%1'> code </font>&nbsp;&nbsp;</font>").arg(
        HLParPluginView::mixColor(m_pColorButtonPB->color(),((m_StepSB->value()/100.0f)*(i+1) <= 1)?(m_StepSB->value()/100.0f)*(i+1):0).name()).arg(
        HLParPluginView::mixColor((!i)?"#000000":m_pColorButtonPB->color(),(((m_StepSB->value()/100.0f)*i)>1)?0:((m_StepSB->value()/100.0f)*i)).name()));
}

// kate: space-indent on; indent-width 2; replace-tabs on;
