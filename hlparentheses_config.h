#ifndef HLPARENTHISES_CONFIG_H
#define HLPARENTHISES_CONFIG_H

#include <kcmodule.h>
#include <QtGui/QWidget>
#include "configpage.h"

class Ui_Config;

class HLParenthesesConfig : public KCModule {
  Q_OBJECT
public:
  HLParenthesesConfig(QWidget* parent, const QVariantList &args);

  virtual void save();
  virtual void load();
  virtual void defaults();

private:
  Ui_Config* ui;
  QList<ConfigPage*> m_lPages;

private slots:
    void slotChanged();
};

#endif
// kate: space-indent on; indent-width 2; replace-tabs on;
