/* This file is part of the KDE libraries
   Copyright (C) 2013 Emmanuel Lepage Vallee <elv1313@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "hlparentheses.h"
#include "hlparentheses_config.h"

#include <QtGui/QApplication>
#include <ktexteditor/movinginterface.h>
#include <ktexteditor/view.h>
#include <ktexteditor/configinterface.h>
#include <kaboutdata.h>

//Global config options
QList<class HLParPluginView*> HighlightParenthesesPlugin::m_views;
KTextEditor::ConfigInterface* HLParPluginView::m_pConfInt;
const char* HLParPluginView::DEFAULT_COLORS[] = {
"#6BFF75", "#FFA95D", "#6B95FF", "#FF6A6A", "#F1FF55",
"#67FFFF", "#DD8CFF", "#775CFF", "#8E8E8E", "#85BF2F"};
const char* HLParPluginView::MODE_PREFIXES[] = {"par","shift","bra"};
const char* HLParPluginView::TOKENS[HL_PAGE_COUNT][2] = {{"(",")"},{"<<","<<"},{"[","]"}};
const int   HLParPluginView::DEFAULT_CONF[HL_PAGE_COUNT][4] ={{12,50,1,8},{12,50,2,8},{0,50,0,8}};


K_PLUGIN_FACTORY_DEFINITION( HighlightParenthesesPluginFactory,
  registerPlugin<HighlightParenthesesPlugin>("ktexteditor_hlparentheses");
  registerPlugin<HLParenthesesConfig>("ktexteditor_hlparentheses_config"); 
)

K_EXPORT_PLUGIN( HighlightParenthesesPluginFactory( KAboutData( "ktexteditor_hlparentheses",
  "ktexteditor_plugins", ki18n("Highlight Parentheses"), "1.0",
  ki18n("Highlight Parentheses"), KAboutData::License_LGPL_V2 ) ) )

HighlightParenthesesPlugin::HighlightParenthesesPlugin( QObject *parent, const QVariantList& )
  : KTextEditor::Plugin ( parent )
{ }
HighlightParenthesesPlugin::~HighlightParenthesesPlugin()
{ }

void HighlightParenthesesPlugin::addView(KTextEditor::View *view)
{
  m_views.append(new HLParPluginView (view));
}

void HighlightParenthesesPlugin::removeView(KTextEditor::View *view)
{
  foreach (HLParPluginView *pluginView, m_views) {
    if (pluginView->m_view == view) {
      m_views.removeAll(pluginView);
      delete pluginView;
      break;
    }
  }
}

void HighlightParenthesesPlugin::reload(){
  foreach (HLParPluginView* view, m_views)
    view->loadConfig();
}

HLParPluginView::HLParPluginView( KTextEditor::View *view)
  : QObject( view ),m_view(view),m_CurRangeStart(0)
{
  setObjectName("highlight-parentheses-plugin");
  m_pConfInt = qobject_cast<KTextEditor::ConfigInterface*>(m_view);
  loadConfig();

  connect(view            , SIGNAL(cursorPositionChanged(KTextEditor::View*,KTextEditor::Cursor)), this, SLOT(selectionChanged()));
  connect(view->document(), SIGNAL(aboutToReload(KTextEditor::Document*))                        , this, SLOT(clearHighlights()) );
}

HLParPluginView::~HLParPluginView()
{
  clearHighlights();
}

void HLParPluginView::clearHighlights()
{
  qDeleteAll(m_ranges);
  m_ranges.clear();
}

void HLParPluginView::startRange(int colorCount, int startPos, int endPos, KTextEditor::Cursor& start,KTextEditor::Cursor& end,char mode)
{
  KTextEditor::MovingInterface* miface = qobject_cast<KTextEditor::MovingInterface*>(m_view->document());
  start.setColumn(startPos);
  end.setColumn(endPos);
  KTextEditor::MovingRange* mr = miface->newMovingRange(KTextEditor::Range(start,end));
  mr->setAttribute(((endPos-startPos==1+(mode==1))?m_attrs:m_attrs_text)[(int)mode][(colorCount>=0 && colorCount < 10)?colorCount:0]);
  mr->setView(m_view);
  mr->setZDepth(-130000.0); // Set the z-depth to slightly worse than the hl selection plugin
  mr->setAttributeOnlyForViews(true);
  m_ranges.append(mr);
}

void HLParPluginView::stopRange(int colorCount, int endPos,KTextEditor::Cursor& start,KTextEditor::Cursor& end, char mode)
{
  if (m_CurRangeStart!=endPos && m_CurRangeStart && colorCount>=0) {
    startRange(colorCount,m_CurRangeStart,endPos,start,end,mode);
    m_CurRangeStart =0;
  }
}

void HLParPluginView::highlightRange(const QString& del1, const QString& del2,char mode)
{
  const int line = m_view->cursorPosition().line();
  const QString content = m_view->document()->line(line);
  KTextEditor::Cursor start(line, 0),end(line,1);
  int colorCount =-1;
  for (int i=0;i<content.count();i++) {
    if ((content.mid(i,del1.size()) == del1) || (content.mid(i,del2.size()) == del2)) {
      stopRange(colorCount,i,start,end,mode);
      colorCount += (content.mid(i,del1.size()) == del1);
      startRange(colorCount, i, i+del1.size(), start,end,mode);
      m_CurRangeStart = i+del1.size();
      i+=del1.size()-1;
      colorCount -= (colorCount<-1)?colorCount:(content.mid(i,del2.size()) == del2)*1;
    }
  }
  stopRange(colorCount,content.count(),start,end,mode);
}

void HLParPluginView::selectionChanged()
{
  clearHighlights();
  for (int mode=0;mode<HL_PAGE_COUNT;mode++)
    if (m_Mode[mode])
      highlightRange(TOKENS[mode][0],TOKENS[mode][1],mode);
}

QColor HLParPluginView::mixColor(const QColor& fg, float pc)
{
  QColor curLineCol = (m_pConfInt)?m_pConfInt->configValue("current-line-color").toString():QApplication::palette().base().color();
  if (!curLineCol.isValid())
    curLineCol = m_pConfInt->configValue("background-color").toString();
  const uchar bgR(curLineCol.red()),bgG(curLineCol.green()),bgB(curLineCol.blue()),fgR(fg.red()),fgG(fg.green()),fgB(fg.blue());
  const short fgRf(fgR*pc*0.94f),fgGf(fgG*pc*0.94f),fgBf(fgB*pc*0.94f);
  const short domC = (((bgR+fgRf)|(bgG+fgGf)|(bgB+fgBf))>0xff)*((fgR>=fgB&&fgR>=fgG)?fgRf:(fgB>=fgR&&fgB>=fgG)?fgBf:fgGf);
  return QColor(bgR+fgRf-domC,bgG+fgGf-domC,bgB+fgBf-domC);
}

void HLParPluginView::loadConfig()
{
  for (int mode=0;mode<HL_PAGE_COUNT;mode++) {
    KConfigGroup cg(KGlobal::config(), "Highlight Parentheses Plugin");
    m_BGOpacity   [mode] = cg.readEntry(MODE_PREFIXES[mode]+QString("_bg_opacity"), DEFAULT_CONF[mode][0] );
    m_FGOpacity   [mode] = cg.readEntry(MODE_PREFIXES[mode]+QString("_fg_opacity"), DEFAULT_CONF[mode][1] );
    m_Mode        [mode] = cg.readEntry(MODE_PREFIXES[mode]+QString("_mode"      ), DEFAULT_CONF[mode][2] );
    m_UnifiedStep [mode] = cg.readEntry(MODE_PREFIXES[mode]+QString("_unif_step" ), DEFAULT_CONF[mode][3] );
    m_UnifiedCol  [mode] = cg.readEntry(MODE_PREFIXES[mode]+QString("_unifcolor" ), "#0051FF"             );

    m_attrs[mode].clear();
    m_attrs_text[mode].clear();
    for (int i=1;i<=10;i++) {
      const QColor col = cg.readEntry(MODE_PREFIXES[mode]+QString("_color")+QString::number(i)  ,DEFAULT_COLORS[i-1]);
      KTextEditor::Attribute::Ptr attrTxt(new KTextEditor::Attribute()),attr(new KTextEditor::Attribute());
      if ( m_Mode[mode] == 1) {
        const QColor colBG(mixColor(col,(m_BGOpacity[mode]/100.0f))),colFG(mixColor(col,(m_FGOpacity[mode]/100.0f)));
        attrTxt->setBackground( colBG );
        attr->   setBackground( colBG );
        attr->   setForeground( colFG );
      }
      else if ( m_Mode[mode] == 2) {
        const QColor colBG = mixColor(m_UnifiedCol[mode],(i*m_UnifiedStep[mode]/100.0f <= 1)?i*(m_UnifiedStep[mode]/100.0f):0);
        attrTxt->setBackground( colBG );
        attr->   setBackground( colBG );
      }
      attr->setFontBold   ( true );
      m_attrs_text[mode] << attrTxt;
      m_attrs     [mode] << attr ;
    }
  }
}

// kate: space-indent on; indent-width 2; replace-tabs on;
