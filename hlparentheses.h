/* This file is part of the KDE libraries
   Copyright (C) 2010 Dominik Haumann <dhaumann kde org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
 */

#ifndef _HIGHLIGHT_PARENTHESES_PLUGIN_H_
#define _HIGHLIGHT_PARENTHESES_PLUGIN_H_

#define HL_PAGE_COUNT 3

//KDE
#include <kxmlguiclient.h>
#include <kpluginfactory.h>

//KTE
#include <ktexteditor/plugin.h>
#include <ktexteditor/attribute.h>

namespace KTextEditor {
  class MovingRange;
  class View;
  class Cursor;
  class ConfigInterface;
}

class HighlightParenthesesPlugin : public KTextEditor::Plugin
{
  Q_OBJECT

  public:
    explicit HighlightParenthesesPlugin( QObject *parent = 0,
                      const QVariantList &args = QVariantList() );
    virtual ~HighlightParenthesesPlugin();

    //Plugin gears
    void addView (KTextEditor::View *view);
    void removeView (KTextEditor::View *view);
    static void reload();

  private:
    static QList<class HLParPluginView*> m_views;
};

class HLParPluginView
  : public QObject, public KXMLGUIClient
{
  Q_OBJECT
  friend class HighlightParenthesesPlugin;
  public:
    //Constructor
    explicit HLParPluginView(KTextEditor::View *view);
    ~HLParPluginView();

    //Mutator
    void loadConfig();

    //Defaults
    static QColor mixColor(const QColor& fg, float pc);
    static const char*  TOKENS        [HL_PAGE_COUNT][2];
    static const int    DEFAULT_CONF  [HL_PAGE_COUNT][4];
    static const char*  DEFAULT_COLORS[];
    static const char*  MODE_PREFIXES [];

  public Q_SLOTS:
    void selectionChanged();
    void clearHighlights();

  private:
    //Attributes
    KTextEditor::View* m_view;
    QList<KTextEditor::Attribute::Ptr> m_attrs     [HL_PAGE_COUNT];
    QList<KTextEditor::Attribute::Ptr> m_attrs_text[HL_PAGE_COUNT];
    QList<KTextEditor::MovingRange*> m_ranges;
    int m_CurRangeStart;

    //Settings
    int    m_Mode          [HL_PAGE_COUNT];
    int    m_BGOpacity     [HL_PAGE_COUNT];
    int    m_FGOpacity     [HL_PAGE_COUNT];
    int    m_UnifiedStep   [HL_PAGE_COUNT];
    QColor m_UnifiedCol    [HL_PAGE_COUNT];
    static KTextEditor::ConfigInterface* m_pConfInt;

    //Helpers
    void startRange(int colorCount,int startPos, int endPos, KTextEditor::Cursor& start,KTextEditor::Cursor& end, char mode);
    inline void stopRange(int colorCount,int endPos,KTextEditor::Cursor& start,KTextEditor::Cursor& end, char mode);
    inline void highlightRange(const QString& del1, const QString& del2, char mode);
};

K_PLUGIN_FACTORY_DECLARATION(HighlightParenthesesPluginFactory)

#endif // _HIGHLIGHT_Parentheses_PLUGIN_H_

// kate: space-indent on; indent-width 2; replace-tabs on;
