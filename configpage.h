/* This file is part of the KDE libraries
   Copyright (C) 2010 Dominik Haumann <dhaumann kde org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
 */

#ifndef _CONFIG_PAGE_H_
#define _CONFIG_PAGE_H_

#include "ui_page.h"

class ConfigPage :  public QWidget, public Ui_ConfigPage {
  Q_OBJECT
public:
  ConfigPage(QWidget* parent = 0);
  QList<KColorButton*> m_lParButtons;
  QList<QLabel*> m_lPreviewLabel;
  void setMarkers(const QString& beg, const QString& end) {
    m_Beg = beg;
    m_End = end;
  }
private:
  QString m_Beg;
  QString m_End;
private slots:
  void slotChanged() {
    emit changed();
  }
public slots:
  void slotReloadPreview();

signals:
  void changed();
};

#endif